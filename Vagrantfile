# -*- mode: ruby -*-
# vi: set ft=ruby :

# plugin
#   vagrant-cachier
#   vagrant-vbguest
#   vagrant-teraterm
#
Vagrant.require_version('>= 1.8.0')

Vagrant.configure(2) do |config|

  version = Gem::Version.new(Vagrant::VERSION)

  config.vm.box = "bento/centos-7.1"

  config.vm.network "private_network", ip: "192.168.33.11", nictype: "virtio"
  config.vm.hostname = "ansible-local.test"

  if Vagrant.has_plugin?("vagrant-vbguest")
    config.vbguest.auto_update = false
    config.vbguest.no_remote = true
  end

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

  config.vm.provider "virtualbox" do |vb|
    # vb.gui = true
    vb.memory = "2048"
    vb.cpus = 2

    vb.linked_clone = true

  end

  if Gem::Requirement.new('>= 1.8.2').satisfied_by?(version) || ARGV[0] == 'up'

    config.vm.provision 'install ansible.yml from epel-testing', type: "ansible_local" do |ansible|
      ansible.provisioning_path = '/vagrant/provisioning'
      ansible.playbook = 'ansible.yml'
      ansible.verbose = true
      ansible.install = true
    end

    if Gem::Requirement.new('<= 1.8.1').satisfied_by?(version)
      # Patch for https://github.com/mitchellh/vagrant/issues/6793
      config.vm.provision "Patch#6793", type: 'shell' do |s|
        s.inline = '[[ ! -f $1 ]] || grep -F -q "$2" $1 || sed -i "/__main__/a \\    $2" $1'
        s.args = ['/usr/bin/ansible-galaxy', "if sys.argv == ['/usr/bin/ansible-galaxy', '--help']: sys.argv.insert(1, 'info')"]
      end
    end

    config.vm.provision 'playbook.yml', type: "ansible_local" do |ansible|
      ansible.verbose = true
      ansible.provisioning_path = '/vagrant/provisioning'
      ansible.playbook = 'playbook.yml'

      ansible.galaxy_role_file = 'requirements.yml'
      if Gem::Requirement.new('<= 1.8.1').satisfied_by?(version)
        ansible.galaxy_command = "cd #{ansible.provisioning_path} && sudo ansible-galaxy install --role-file=#{ansible.galaxy_role_file} --force"
      end
    end
  end
end

